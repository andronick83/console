<?

function get_composer_autoload() {
    $depth = 0;
    while ( TRUE ) {
        $autoload = __DIR__.str_repeat('/..', $depth).'/vendor/autoload.php';
        if ( is_file($autoload) ) { return $autoload; break; }
        if ( $depth++ > 5 ) { error_log('Don\'t find composer-autoload'); exit(); }
    }
}
require_once get_composer_autoload();

use \Console\Console;
use \Console\Color;


function print_console_styles(){
    echo PHP_EOL;
    //
    $ansi_keys = array_keys(Color::$ANSI_COLORS);
    echo 'Defined styles: '.implode(', ', $ansi_keys).PHP_EOL;
    //
    if ( !IS_CLI ) {
        echo Color::css_tag().PHP_EOL;
        echo '<pre class="console">'.PHP_EOL;
    }
    //
    function test_out($key) {
        echo Color::set('Test styled string for key: '.$key, $key).PHP_EOL;
    }
    $test_out = 'test_out';
    //
    // Test font-styles
    $test_out('off');
    $test_out('underline');
    echo PHP_EOL;
    //
    // Test font-colors
    $test_out('black silver-bg');
    $test_out('gray');
    $test_out('maroon');
    $test_out('red');
    $test_out('green');
    $test_out('lime');
    $test_out('olive');
    $test_out('yellow');
    $test_out('navy');
    $test_out('blue');
    $test_out('purple');
    $test_out('magenta');
    $test_out('teal');
    $test_out('cyan');
    $test_out('silver');
    $test_out('white');
    echo PHP_EOL;
    //
    // Test background-colors
    $test_out('white black-bg');
    $test_out('white gray-bg');
    $test_out('white maroon-bg');
    $test_out('black red-bg');
    $test_out('black green-bg');
    $test_out('black lime-bg');
    $test_out('black olive-bg');
    $test_out('black yellow-bg');
    $test_out('white navy-bg');
    $test_out('black blue-bg');
    $test_out('white purple-bg');
    $test_out('black magenta-bg');
    $test_out('black teal-bg');
    $test_out('black cyan-bg');
    $test_out('black silver-bg');
    $test_out('black white-bg');
    echo PHP_EOL;
    //
    // Test Console out-types
    Console::log('Test log message');
    Console::debug('Test debug message');
    Console::info('Test info message');
    Console::error('Test error message');
    Console::warn('Test warn message');
    Console::out('Test out success message', 'success');
    echo PHP_EOL;
    //
    // Test more data-types
    // - object
    class ConsoleTestClass {
        public $public_var = TRUE;
        private static $private_var = FALSE;
        protected static $protected_var = FALSE;
        public static $array_var = array(1, 2, 3);
        public function __construct() {
            self::$private_var = "TRUE";
            self::$protected_var = 1;
            return 'public method return';
        }
        public static function public_method() { return 'public method return'; }
        private static function private_method() { return 'private method return'; }
        protected static function protected_method() { return 'protected method return'; }
    }
    $testclass = new ConsoleTestClass;
    Console::log('Test object output', $testclass);
    unset($testclass);
    // - resource
    $testresource = tmpfile();
    Console::log('Test resource output', $testresource);
    fclose($testresource);
    // - function
    $testfunc = create_function('$arg', 'return strtoupper($arg);');
    Console::log('Test function output', $testfunc);
    unset($testfunc);
    // - array
    Console::log(array('Test array output', 1, 2, array(1, 2, 3)));
    // - assoc-array
    Console::log(array('key' => 'Test assoc-array output', 'key2' => array(1, 2, 3)));
    //
    if ( !IS_CLI ) echo '</pre>'.PHP_EOL;
    else echo PHP_EOL;
}


print_console_styles();

//-
