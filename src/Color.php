<?

namespace Console;

require_once __DIR__.'/init.php';


$CONSOLE_ANSI_COLORS = array();
$CONSOLE_ANSI_COLORS['off'] = '0';
$CONSOLE_ANSI_COLORS['bold'] = '1'; // *
$CONSOLE_ANSI_COLORS['italic'] = '3'; // *
$CONSOLE_ANSI_COLORS['underline'] = '4';
$CONSOLE_ANSI_COLORS['blink'] = '5'; // *
$CONSOLE_ANSI_COLORS['inverse'] = '7'; // *
$CONSOLE_ANSI_COLORS['hidden'] = '8'; // *

$CONSOLE_ANSI_COLORS['black'] = '30';
$CONSOLE_ANSI_COLORS['gray'] =
$CONSOLE_ANSI_COLORS['low-gray'] = '1;30';
$CONSOLE_ANSI_COLORS['black-bg'] = '40';
$CONSOLE_ANSI_COLORS['gray-bg'] =
$CONSOLE_ANSI_COLORS['low-gray-bg'] = '5;40';

$CONSOLE_ANSI_COLORS['maroon'] =
$CONSOLE_ANSI_COLORS['low-red'] = '31';
$CONSOLE_ANSI_COLORS['red'] =
$CONSOLE_ANSI_COLORS['hi-red'] = '1;31';
$CONSOLE_ANSI_COLORS['maroon-bg'] =
$CONSOLE_ANSI_COLORS['low-red-bg'] = '41';
$CONSOLE_ANSI_COLORS['red-bg'] =
$CONSOLE_ANSI_COLORS['hi-red-bg'] = '5;41';

$CONSOLE_ANSI_COLORS['green'] =
$CONSOLE_ANSI_COLORS['low-green'] = '32';
$CONSOLE_ANSI_COLORS['lime'] =
$CONSOLE_ANSI_COLORS['hi-green'] = '1;32';
$CONSOLE_ANSI_COLORS['green-bg'] =
$CONSOLE_ANSI_COLORS['low-green-bg'] = '42';
$CONSOLE_ANSI_COLORS['lime-bg'] =
$CONSOLE_ANSI_COLORS['hi-green-bg'] = '5;42';

$CONSOLE_ANSI_COLORS['olive'] =
$CONSOLE_ANSI_COLORS['low-yellow'] = '33';
$CONSOLE_ANSI_COLORS['yellow'] =
$CONSOLE_ANSI_COLORS['hi-yellow'] = '1;33';
$CONSOLE_ANSI_COLORS['olive-bg'] =
$CONSOLE_ANSI_COLORS['low-yellow-bg'] = '43';
$CONSOLE_ANSI_COLORS['yellow-bg'] =
$CONSOLE_ANSI_COLORS['hi-yellow-bg'] = '5;43';

$CONSOLE_ANSI_COLORS['navy'] =
$CONSOLE_ANSI_COLORS['low-blue'] = '34';
$CONSOLE_ANSI_COLORS['blue'] =
$CONSOLE_ANSI_COLORS['hi-blue'] = '1;34';
$CONSOLE_ANSI_COLORS['navy-bg'] =
$CONSOLE_ANSI_COLORS['low-blue-bg'] = '44';
$CONSOLE_ANSI_COLORS['blue-bg'] =
$CONSOLE_ANSI_COLORS['hi-blue-bg'] = '5;44';

$CONSOLE_ANSI_COLORS['purple'] =
$CONSOLE_ANSI_COLORS['low-magenta'] = '35';
$CONSOLE_ANSI_COLORS['magenta'] =
$CONSOLE_ANSI_COLORS['hi-magenta'] =
$CONSOLE_ANSI_COLORS['fuchsia'] = '1;35';
$CONSOLE_ANSI_COLORS['purple-bg'] =
$CONSOLE_ANSI_COLORS['low-magenta-bg'] = '45';
$CONSOLE_ANSI_COLORS['magenta-bg'] =
$CONSOLE_ANSI_COLORS['hi-magenta-bg'] =
$CONSOLE_ANSI_COLORS['fuchsia-bg'] = '5;45';

$CONSOLE_ANSI_COLORS['teal'] =
$CONSOLE_ANSI_COLORS['low-cyan'] = '36';
$CONSOLE_ANSI_COLORS['cyan'] =
$CONSOLE_ANSI_COLORS['hi-cyan'] =
$CONSOLE_ANSI_COLORS['aqua'] = '1;36';
$CONSOLE_ANSI_COLORS['teal-bg'] =
$CONSOLE_ANSI_COLORS['low-cyan-bg'] = '46';
$CONSOLE_ANSI_COLORS['cyan-bg'] =
$CONSOLE_ANSI_COLORS['hi-cyan-bg'] =
$CONSOLE_ANSI_COLORS['aqua-bg'] = '5;46';

$CONSOLE_ANSI_COLORS['silver'] =
$CONSOLE_ANSI_COLORS['hi-gray'] = '37';
$CONSOLE_ANSI_COLORS['white'] = '1;37';
$CONSOLE_ANSI_COLORS['silver-bg'] =
$CONSOLE_ANSI_COLORS['hi-gray-bg'] = '47';
$CONSOLE_ANSI_COLORS['white-bg'] = '5;47';


$CONSOLE_CSS_COLORS = <<<EOF
.console{padding:1px;line-height:16px;font-family:Courier New;font-size:14px;}
.out{margin:0;padding:0;}
.console,
.out.off{background-color:#000;color:#bbb;}
.out.bold{font-weight:bold;} /***/
.out.italic{font-style:italic;} /***/
.out.underline{text-decoration:underline;}
.out.blink{text-decoration:blink;} /***/
.out.inverse{} /***/
.out.hidden{} /***/

.out.black{color:#000;}
.out.gray,
.out.low-gray{color:#555;}
.out.black-bg{background-color:#000;}
.out.gray-bg,
.out.low-gray-bg{background-color:#555;}

.out.maroon,
.out.low-red{color:#b00;}
.out.red,
.out.hi-red{color:#f55;}
.out.maroon-bg,
.out.low-red-bg{background-color:#b00;}
.out.red-bg,
.out.hi-red-bg{background-color:#f55;}

.out.green,
.out.low-green{color:#0b0;}
.out.lime,
.out.hi-green{color:#5f5;}
.out.green-bg,
.out.low-green-bg{background-color:#0b0;}
.out.lime-bg,
.out.hi-green-bg{background-color:#5f5;}

.out.olive,
.out.low-yellow{color:#bb0;}
.out.yellow,
.out.hi-yellow{color:#ff5;}
.out.olive-bg,
.out.low-yellow-bg{background-color:#bb0;}
.out.yellow-bg,
.out.hi-yellow-bg{background-color:#ff5;}

.out.navy,
.out.low-blue{color:#00b;}
.out.blue,
.out.hi-blue{color:#55f;}
.out.navy-bg,
.out.low-blue-bg{background-color:#00b;}
.out.blue-bg,
.out.hi-blue-bg{background-color:#55f;}

.out.purple,
.out.low-magenta{color:#b0b;}
.out.magenta,
.out.hi-magenta,
.out.fuchsia{color:#f5f;}
.out.purple-bg,
.out.low-magenta-bg{background-color:#b0b;}
.out.magenta-bg,
.out.hi-magenta-bg,
.out.fuchsia-bg{background-color:#f5f;}

.out.teal,
.out.low-cyan{color:#0bb;}
.out.cyan,
.out.hi-cyan,
.out.aqua{color:#5ff;}
.out.teal-bg,
.out.low-cyan-bg{background-color:#0bb;}
.out.cyan-bg,
.out.hi-cyan-bg,
.out.aqua-bg{background-color:#5ff;}

.out.silver,
.out.hi-gray{color:#bbb;}
.out.white{color:#fff;}
.out.silver-bg,
.out.hi-gray-bg{background-color:#bbb;}
.out.white-bg{background-color:#fff;}
EOF;


/**
 * echo \Console\Color::set("Error", "red+bold") . PHP_EOL;
 * echo \Console\Color::set("Success", "green,bold") . " Something was successful!" . PHP_EOL;
 * echo \Console\Color::set("Error", "red underline") . PHP_EOL;
 */
class Color {
    public static $ANSI_COLORS = NULL;
    public static $CSS_COLORS = NULL;

    public function __construct() { }

    public static function set($str, $style) {
        $class = trim(preg_replace('/[,;\.\+\s]+/', ' ', $style));
        if ( !IS_CLI ) return '<span class="out '.$class.'">'.$str.'</span>';
        if ( !USE_ANSI ) return $str;
        $class = explode(' ', $class);
        $ansi = ''; foreach ( $class as $key ) $ansi.= "\033[".self::$ANSI_COLORS[$key].'m';
        return $ansi.$str."\033[".self::$ANSI_COLORS['off'].'m';
    }
    public static function get_css($minify_css = TRUE) {
        $css = self::$CSS_COLORS;
        if ( $minify_css ) {
            $css = preg_replace('#/\*.*?\*/#s', '', $css); // Remove comments
            $css = preg_replace('/\s*([{}|:;,])\s+/', '$1', $css); // Remove whitespace
            $css = preg_replace('/\s\s+(.*)/', '$1', $css); // Remove trailing whitespace at the start
            $css = str_replace(';}', '}', $css); // Remove unnecesairy ;'s
        }
        return $css;
    }
    public static function css_tag($minify_css = TRUE) {
        return '<style type="text/css" data-name="console-out">'.self::get_css($minify_css).'</style>';
    }
}


Color::$ANSI_COLORS = $CONSOLE_ANSI_COLORS;
Color::$CSS_COLORS = $CONSOLE_CSS_COLORS;
