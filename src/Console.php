<?

namespace Console;
use \Console\Color;

require_once __DIR__.'/init.php';


/**
 * \Console\Console::log('string', .5, array('key' => 'value'), TRUE);
 * use Console\Console;
 * Console::log('log message');
 * Console::info('info message');
 * Console::error('error message');
 * Console::warn('warn message');
 * Console::debug('debug message');
 */
class Console {
    public static $STYLES = array(
        'log' => 'silver',
        'debug' => 'white',
        'info' => 'yellow',
        'error' => 'red',
        'warn' => 'blue',
        'success' => 'lime',

        'var-default' => 'silver',
        'var-string' => 'red',
        'var-bool' => 'blue',
        'var-numeric' => 'blue',
        'var-null' => 'gray',
        'var-undefined' => 'gray',
        'var-key' => 'purple',
        'var-class' => 'white',
    );

    public function __construct() { }

    public static function out($str, $type, $newLine = TRUE) {
        echo self::set_style($str, $type).( $newLine ? ( IS_CLI ? PHP_EOL : "\n" ) : '' );
    }
    public static function log() { self::out(self::args2str(func_get_args(), 'log'), 'log', TRUE); }
    public static function debug() { self::out(self::args2str(func_get_args(), 'debug'), 'debug', TRUE); }
    public static function info() { self::out(self::args2str(func_get_args(), 'info'), 'info', TRUE); }
    public static function error() { self::out(self::args2str(func_get_args(), 'error'), 'error', TRUE); }
    public static function warn() { self::out(self::args2str(func_get_args(), 'warn'), 'warn', TRUE); }

    private static function var2str(&$var) {
        if ( !isset($var) ) return self::set_style('undefined', 'var-undefined');
        if ( is_null($var) ) return self::set_style(json_encode($var, JSON_UNESCAPED_UNICODE), 'var-null');
        if ( is_bool($var) ) return self::set_style(json_encode($var, JSON_UNESCAPED_UNICODE), 'var-bool');
        if ( is_string($var) ) return self::set_style(json_encode($var, JSON_UNESCAPED_UNICODE), 'var-string');
        if ( is_numeric($var) ) return self::set_style(json_encode($var, JSON_UNESCAPED_UNICODE), 'var-numeric');

        if ( is_callable($var, TRUE, $name) ) return self::set_style('function', 'var-class').' '.$name;
        if ( is_array($var) ) return self::set_style('Array', 'var-class').'['.count($var).']';
        if ( is_object($var) ) return self::set_style(get_class($var), 'var-class');
        if ( is_resource($var) ) return self::set_style('Resource #'.intval($var), 'var-class').' <'.get_resource_type($var).'>';
        //
        return self::set_style(json_encode($var, JSON_UNESCAPED_UNICODE), 'var-default');
    }
    private static function arr2str(&$arr) {
        if ( (bool)count(array_filter(array_keys($arr), 'is_string')) ) $assoc = TRUE; else $assoc = FALSE;
        $ret = array();
        while ( list($key, $value) = each($arr) ) {
            if ( !isset($arr[$key]) ) continue;
            $str = self::var2str($arr[$key]);
            if ( $assoc ) {
                $aux = explode("\0", $key);
                $newkey = $aux[count($aux)-1];
                $str = self::set_style($newkey, 'var-key').': '.$str;
            }
            $ret[] = $str;
        }
        unset($str, $key, $value);
        $ret = implode(', ', $ret);
        if ( $assoc ) $ret = '{'.$ret.'}'; else $ret = '['.$ret.']';
        return $ret;
    }
    private static function obj2str(&$obj) {
        $ret = array();

        $clone = (array)$obj;
        while ( list($key, $value) = each($clone) ) {
            if ( !isset($clone[$key]) ) continue;
            $aux = explode("\0", $key);
            $newkey = $aux[count($aux)-1];
            $ret[] = self::set_style($newkey, 'var-key').': '.self::var2str($clone[$key]);
        }

        $class = get_class($obj);
        $class_vars = get_class_vars($class);
        while ( list($key, $value) = each($class_vars) ) {
            if ( isset($clone[$key]) ) continue;
            $aux = explode("\0", $key);
            $newkey = $aux[count($aux)-1];
            $ret[] = self::set_style($newkey, 'var-key').': '.self::var2str($class_vars[$key]);
        }

        $class_methods = get_class_methods($class);
        while ( list($index, $key) = each($class_methods) ) {
            if ( isset($clone[$key]) ) continue;
            $aux = explode("\0", $key);
            $newkey = $aux[count($aux)-1];
            $ret[] = self::set_style($newkey, 'var-key').': function';
        }

        unset($clone, $class_vars, $class_methods, $key, $value, $index);
        $ret = self::set_style($class, 'var-class').' {'.implode(', ', $ret).'}';
        return $ret;
    }
    private static function args2str($arglist, $defKey = 'var-default') {
        if ( count($arglist) === 0 ) return FALSE;
        $firstarg = gettype($arglist[0]);
        $outlist = array();
        foreach ( $arglist as $arg ) {
            $argtype = gettype($arg);
            switch($argtype){
                case 'string':
                    if ( $firstarg !== 'string' )
                        $outlist[] = self::set_style(json_encode($arg, JSON_UNESCAPED_UNICODE), 'var-string');
                    else
                        $outlist[] = self::set_style($arg, $defKey);
                    break;
                case 'array':
                    $outlist[] = self::arr2str($arg);
                    break;
                case 'object':
                    $outlist[] = self::obj2str($arg);
                    break;
                case 'function':
                case 'NULL':
                case 'boolean':
                case 'integer':
                case 'double':
                case 'unknown type':
                case 'resource':
                default:
                    $outlist[] = self::var2str($arg);
                    break;
            }
        }
        if ( $firstarg !== 'string' )
            $out = implode(', ', $outlist);
        else
            $out = implode(' ', $outlist);
        return $out;
    }
    private static function set_style($str, $type = NULL) {
        if ( empty($type) ) return Color::set($str, self::$STYLES['default']);
        if ( isset( self::$STYLES[$type] ) ) return Color::set($str, self::$STYLES[$type]);
        return Color::set($str, $type);
    }
}
