<?

global $argv;

if ( !defined('IS_CLI') ) define('IS_CLI', php_sapi_name() === 'cli');

if ( IS_CLI && !defined('USE_ANSI') ) {
    if ( in_array('--no-ansi', $argv)) define('USE_ANSI', FALSE);
    elseif ( in_array('--ansi', $argv)) define('USE_ANSI', TRUE);
    else
        // On Windows, default to no ANSI, except in ANSICON and ConEmu.
        // Everywhere else, default to ANSI if stdout is a terminal.
        define('USE_ANSI', (DIRECTORY_SEPARATOR == '\\') ?
            ( getenv('ANSICON') !== FALSE || getenv('ConEmuANSI') === 'ON' ) :
            ( function_exists('posix_isatty') && posix_isatty(1) )
        );
}

//-
