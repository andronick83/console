# README #

* PHP and PHP_CLI colored console messaging
* v1.0.0
* [GIT repo](https://bitbucket.org/tutorials/markdowndemo)


### Installing ###

Preferred way to install is with Composer.
Just add in your projects composer.json:

```
#!json

"require": {
  "andronick83/console": "*"
}
```


### Usage: ###

```
#!php
<?php

require('vendor/autoload.php');
use Console\Console;
use Console\Color;

// Output colored messages
Console::log('Test log message');
Console::debug('Test debug message');
Console::info('Test info message');
Console::error('Test error message');
Console::warn('Test warn message');
Console::out('Test out success message', 'success');

// Output more variable-types
Console::log("string", .5, array(1, 2, 3, array(1, 2, 3)), array("key" => "value"));

// Get colored string
$str = Color::set('string', 'blue white-bg');
$str2 = Color::set('string', 'yellow underline');

//-

```